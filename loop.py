import random

import numpy as np

import hallway
import pedestrian


class Loop:
    def __init__(self, width, length, door_width, direction_ratio=1.0,
                 n_create=1, obstacle='none'):
        self.hallway = hallway.Hallway(width, length, door_width, obstacle)
        self.pedestrians = []
        self.density = np.zeros([width, length])
        self.density[self.hallway.walls == 1] = pedestrian.MAX_DENSITY
        self.direction_ratio = direction_ratio

        self.queue = []
        self.time = 0
        self.n_create = n_create

    def create_pedestrian(self, direction):
        x = np.random.randint(self.hallway.width)
        if direction > 0:
            y = 0
        else:
            y = self.hallway.length - 1

        p = pedestrian.Pedestrian(np.array([x, y]), direction)

        return p

    def add_pedestrians(self):
        created = []

        for p in self.queue:
            if self.density[p.grid_point()] < pedestrian.MAX_DENSITY:
                self.pedestrians.append(p)
                self.density[p.grid_point()] += 1
                created.append(p)

        for p in created:
            self.queue.remove(p)

    def update(self):
        finished = []

        for p in self.pedestrians:
            if p.direction == 1 and p.grid_point()[1] == self.hallway.length - 1:
                finished.append(p)
            elif p.direction == -1 and p.grid_point()[1] == 0:
                finished.append(p)

        for p in finished:
            self.density[p.grid_point()] -= 1
            self.pedestrians.remove(p)

        for p in self.pedestrians:
            p.update(self.density, self.hallway)

        for _ in range(self.n_create):
            direction = np.random.choice([1, -1], p=[self.direction_ratio, 1 - self.direction_ratio])
            p = self.create_pedestrian(direction)
            self.queue.append(p)

        self.add_pedestrians()

        random.shuffle(self.pedestrians)

        return len(finished)

import numpy as np


MAX_DENSITY = 2


class Pedestrian:
    def __init__(self, position, direction):
        self.position = position
        self.direction = direction
        self.time = 0

    def grid_point(self):
        return int(self.position[0]), int(self.position[1])

    def move(self, pos_new, density, hallway):
        x, y = np.rint(pos_new).astype(int)
        if x < 0 or x > hallway.width - 1:
            return False
        if y < 0 or y > hallway.length - 1:
            return False
        if hallway.walls[x, y]:
            return False
        if density[x, y] >= MAX_DENSITY:
            return False

        density[self.grid_point()] -= 1
        self.position = np.array([x, y])
        density[self.grid_point()] += 1

        return True

    def gradient(self, density):
        x, y = self.grid_point()
        grad = np.zeros(2)

        if x < density.shape[0] - 1:
            grad[0] += 0.5 * density[x + 1, y]
        else:
            grad[0] += 0.5 * MAX_DENSITY
        if x > 0:
            grad[0] -= 0.5 * density[x - 1, y]
        else:
            grad[0] -= 0.5 * MAX_DENSITY

        if y < density.shape[1] - 1:
            grad[1] += 0.5 * density[x, y + 1]
        else:
            grad[1] += 0.5 * MAX_DENSITY
        if y > 0:
            grad[1] -= 0.5 * density[x, y - 1]
        else:
            grad[1] -= 0.5 * MAX_DENSITY

        return grad

    def update(self, density, hallway):
        self.time += 1

        if density[self.grid_point()] >= MAX_DENSITY:
            grad = self.gradient(density)
            pos_new = self.position - grad
            if self.move(pos_new, density, hallway):
                return

        if self.direction == 1:
            path = hallway.path_forward
        else:
            path = hallway.path_backward

        x0, y0 = self.grid_point()
        goal = path[x0, y0] - 1

        if y0 > 0 and path[x0, y0 - 1] == goal:
            pos_new = np.array([x0, y0 - 1])
            if self.move(pos_new, density, hallway):
                return

        if y0 < hallway.length - 1 and path[x0, y0 + 1] == goal:
            pos_new = np.array([x0, y0 + 1])
            if self.move(pos_new, density, hallway):
                return

        if x0 < hallway.width - 1 and path[x0 + 1, y0] == goal:
            pos_new = np.array([x0 + 1, y0])
            if self.move(pos_new, density, hallway):
                return

        if x0 > 0 and path[x0 - 1, y0] == goal:
            pos_new = np.array([x0 - 1, y0])
            if self.move(pos_new, density, hallway):
                return

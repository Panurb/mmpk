import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import loop
import pedestrian


def animate(width, length, door_width, direction_ratio, obstacle, save=False):
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, bitrate=1800)

    instance = loop.Loop(width, length, door_width, direction_ratio, 1, obstacle)

    fig, ax = plt.subplots()
    imshow = plt.imshow(instance.density.T, origin='lower', vmin=0, vmax=pedestrian.MAX_DENSITY,
                        animated=True, interpolation='nearest', cmap='plasma')

    instance.hallway.draw(ax)

    def f(*args):
        instance.update()
        imshow.set_data(instance.density.T)

        return imshow,

    ani = animation.FuncAnimation(fig, f, interval=100, blit=False)
    plt.xlabel('x')
    plt.ylabel('y')

    if save:
        ani.save('ani.mp4', writer=writer)
    else:
        plt.show()


def plot_flow_rate_vs_door_width(width, length, obstacle):
    door_widths = np.arange(1, width + 1, 2)
    direction_ratios = [1, 0.9, 0.7, 0.5]

    for direction_ratio in direction_ratios:
        flow_rates = np.zeros(len(door_widths))

        for i, door_width in enumerate(door_widths):
            print('w =', door_width)
            for n_create in range(1, width):
                instance = loop.Loop(width, length, door_width, direction_ratio, n_create, obstacle)

                # Equilibrate
                for _ in range(200):
                    instance.update()

                # Sample
                time = 0
                finished = 0
                for _ in range(200):
                    time += 1
                    finished += instance.update()

                flow_rate = finished / time
                print('   ', flow_rate)

                if flow_rate < flow_rates[i] or flow_rate == 0:
                    break

                flow_rates[i] = max(flow_rate, flow_rates[i])

        plt.plot(door_widths, flow_rates)

    plt.xlabel('Oven leveys $d$ (ruutua)')
    plt.ylabel('Jalankulkijavirta $q$ (jk / aika-askel)')
    plt.legend(direction_ratios, title='Suuntajakauma $p$')
    plt.savefig('flow_rate_cell.pdf')

    plt.show()


def plot_pathfinding(width, height, door_width):
    instance = loop.Loop(width, height, door_width)

    fig, ax = plt.subplots(figsize=(3, 6))
    plt.xlim((0, width))
    plt.ylim((0, height))
    ax.set_xticks(range(0, width))
    ax.set_yticks(range(0, height))

    plt.grid()

    instance.hallway.draw(ax, 0)

    for x in range(width):
        for y in range(height):
            plt.text(x + 0.25, y + 0.25, int(instance.hallway.path_forward[x, y]), fontsize=8)

    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig('path_finding.pdf')
    plt.show()


def plot_snapshots(width, length, door_width, step, t_max):
    instance = loop.Loop(width, length, door_width, n_create=width)

    fig, ax = plt.subplots()
    imshow = plt.imshow(instance.density.T, origin='lower', vmin=0, vmax=pedestrian.MAX_DENSITY,
                        animated=True, interpolation='nearest', cmap='plasma')
    cbar = plt.colorbar()
    cbar.set_label('Jalankulkijatiheys $k$')

    plt.xlabel('$x$')
    plt.ylabel('$y$')

    instance.hallway.draw(ax)
    for t in range(t_max):
        instance.update()
        if t % step == 0:
            imshow.set_data(instance.density.T)
            plt.savefig('snapshot_cell_' + str(t) + '.png')


def plot_flow_rate_with_obstacles(width, length, direction_ratio):
    door_widths = np.arange(1, width + 1, 2)
    obstacles = ['none', 'box', 'horizontal', 'vertical', 'funnel']

    for obstacle in obstacles:
        flow_rates = np.zeros(len(door_widths))

        for i, door_width in enumerate(door_widths):
            print('w =', door_width)
            for n_create in range(1, width):
                instance = loop.Loop(width, length, door_width, direction_ratio, n_create, obstacle)

                # Equilibrate
                for _ in range(200):
                    instance.update()

                # Sample
                time = 0
                finished = 0
                for _ in range(200):
                    time += 1
                    finished += instance.update()

                flow_rate = finished / time
                print('   ', flow_rate)

                if flow_rate < flow_rates[i] or flow_rate == 0:
                    break

                flow_rates[i] = max(flow_rate, flow_rates[i])

        plt.plot(door_widths, flow_rates)

    plt.xlabel('Oven leveys $d$ (ruutua)')
    plt.ylabel('Jalankulkijavirta $q$ (jk / aika-askel)')
    plt.legend(['Ei mitään', 'Laatikko', 'Yhdensuuntainen', 'Kohtisuora', 'Kouru'], title='Este')
    plt.savefig('flow_rate_obstacle_cell.pdf')
    plt.show()


def plot_obstacles(width, length, door_width):
    obstacles = ['box', 'horizontal', 'vertical', 'funnel']

    for obstacle in obstacles:
        instance = loop.Loop(width, length, door_width, obstacle=obstacle)

        fig, ax = plt.subplots(figsize=(7*width/length, 7))
        imshow = plt.imshow(instance.density.T, origin='lower', vmin=0, vmax=pedestrian.MAX_DENSITY,
                            animated=True, interpolation='nearest', cmap='Greys')

        plt.xlabel('$x$')
        plt.ylabel('$y$')

        instance.hallway.draw(ax)

        plt.savefig('obstacle_cell_' + obstacle + '.png')


plot_obstacles(11, 21, 3)
#plot_flow_rate_with_obstacles(11, 21, 1)
#plot_snapshots(11, 21, 3, 5, 25)
#plot_pathfinding(11, 21, 3)
#plot_flow_rate_vs_door_width(11, 21, 'none')
#animate(11, 21, 3, 0.5, 'funnel')
#flow_rates(21, 41, 3)

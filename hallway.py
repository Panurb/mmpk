import numpy as np
import matplotlib.patches as patches


class Hallway:
    def __init__(self, width, length, door_width, obstacle):
        self.width = width
        self.length = length
        self.door_width = width

        self.walls = np.zeros([width, length])
        middle = length // 2
        frame = (width - door_width) // 2
        self.walls[:frame, middle] = 1
        self.walls[width - frame:, middle] = 1

        if obstacle == 'funnel':
            for i in range(middle - frame, middle):
                for j in range(frame - (middle - i)):
                    self.walls[j, i] = 1
                    self.walls[-j - 1, i] = 1
        elif obstacle == 'horizontal':
            self.walls[width//3:2*width//3 + 1, middle - 3] = 1
        elif obstacle == 'box':
            self.walls[width//2-1:width//2+2, middle-5:middle-2] = 1
        elif obstacle == 'vertical':
            self.walls[width//2, width//3:2*width//3 + 1] = 1

        self.path_forward = self.generate_pathfinding(length - 1)
        self.path_backward = self.generate_pathfinding(0)

    def generate_pathfinding(self, start_y):
        path = -1 * np.ones([self.width, self.length])
        path[:, start_y] = 0

        new = True
        i = 0
        while new:
            new = False
            for x in range(path.shape[0]):
                for y in range(path.shape[1]):
                    if path[x, y] != -1 or self.walls[x, y]:
                        continue

                    if x < path.shape[0] - 1:
                        if path[x + 1, y] == i:
                            path[x, y] = i + 1
                            new = True
                    if x > 0:
                        if path[x - 1, y] == i:
                            path[x, y] = i + 1
                            new = True
                    if y < path.shape[1] - 1:
                        if path[x, y + 1] == i:
                            path[x, y] = i + 1
                            new = True
                    if y > 0:
                        if path[x, y - 1] == i:
                            path[x, y] = i + 1
                            new = True
            i += 1

        return path

    def draw(self, ax, offset=0.5):
        for x in range(self.width):
            for y in range(self.length):
                if self.walls[x, y]:
                    rect = patches.Rectangle((x - offset, y - offset), 1.01, 1.01, facecolor='black')
                    ax.add_patch(rect)
